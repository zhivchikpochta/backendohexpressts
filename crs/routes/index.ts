import * as express from 'express';
import * as path from 'path';
// import { UserJwt } from '@/models/User';
import * as cookieParser from 'cookie-parser';
import { cacheControlMiddleware, parseAuthorizationMiddleware, endPoint } from './lib';
// import acl from '../lib/acl';
// import { UserRole } from '@prisma/client';

const router = express.Router();

declare global {
    namespace Express {
      interface Request {
        authorization: UserJwt;
        language: string | undefined;
      }
    }
  }
  
  router.use(express.urlencoded({ extended: false }));
  router.use(express.json());
  router.use(cookieParser());

function apiRouter(){
    const apiRout = express.Router();

    apiRout.use(cacheControlMiddleware);
    apiRout.use(parseAuthorizationMiddleware);
}
// POST запросы

/* router.post('/registration', controller.registration);
router.post('/login', controller.login);
router.post('/users', controller.getUsers); */

// Публичные страницы
router.get('/');
router.get('/reg');
router.get('/login');
router.get('/items');
router.get('/items/:uid');
router.get('/users');
router.get('/users/:id');

// Страницы для авторизированного пользователя
router.get('/item_create');
router.get('/user');
router.get('/user/items');
router.get('//user/items/:uid');

module.exports = router;